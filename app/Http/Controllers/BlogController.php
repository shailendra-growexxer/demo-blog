<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Blog;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class BlogController extends Controller
{
    /**
     * Display a listing of the blogs.
     */
    public function index()
    {
        
        $blogs = Blog::with('blog_comments')->where('added_by',Auth::user()->id)->orderBy('id','desc')->get();  
        return view('blog.list', ['blogs' => $blogs]);
    }

    /**
     * Display a listing of the all user's blogs.
     */
    public function show_all_blogs(Request $request)
    {
        $categoryCount = Category::count();
        if ($categoryCount == 0)
        {
            $newCategory = new Category;
            $newCategory->category = "Travel";
            $newCategory->save();

            $newCategory = new Category;
            $newCategory->category = "Food";
            $newCategory->save();
        }
        $search = $request->search;
        if ($search) 
        {   
            $blogs = Blog::with('blog_comments')->where(function ($query) use($search) {
                $query->where('title', 'like', '%' . $search . '%')
                   ->orWhere('place', 'like', '%' . $search . '%')
                   ->orWhere('categories', 'like', '%' . $search . '%')
                   ->orWhere('tags', 'like', '%' . $search . '%');
              })->orderBy('id','desc')->paginate(5);  
        } else 
        {
            $blogs = Blog::with('blog_comments')->orderBy('id','desc')->paginate(5);  
        }
        


        return view('dashboard', ['blogs' => $blogs]);
    }

    /**
     * Show the form for creating a new blog.
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created blog in storage.
     */
    public function store(Request $request)
    {
      
        $request->validate([
            'title' => 'required|string',
            'description' => 'required',
            'place' => 'required|string',
            /* 'image' => 'mimes:jpeg,jpg,png,|max:20480', */
        ]);

      /*   $newBlog = Blog::create($request->all());

        return redirect(route('blog.index'));

        */


        $newBlog = new Blog;
        $newBlog->title = $request->title;
        $newBlog->description = $request->description;
        $newBlog->place = $request->place; 
        $newBlog->links = $request->links;
        $newBlog->added_by = $request->added_by;
        
        if ($request->categories) 
        {
            $newBlog->categories = $request->categories;
        }
        if ($request->tags) 
        {
            $newBlog->tags = $request->tags;

            $tagList = explode(',', $request->tags);
            foreach ($tagList as $tag)
            {
                $isTagExists = Tag::where('tag', trim($tag))->exists();
                if (!$isTagExists)
                {
                    Tag::create([
                        'tag'=> trim($tag),
                    ]);
                }
                
            }
        }
      
        if ($request->file('image'))
        {
            $file = $request->image;
            $filename = uniqid(). '.' .$request->file('image')->getClientOriginalExtension();
            //uniqid() is php function to generate uniqid but you can use time() etc.
    
            $path = "public/Files/Images/";
            Storage::disk('local')->put($path.$filename,file_get_contents($file));
            $link = 'Files/Images/'.$filename;
            $newBlog->image = $link;
        }
        

        if ($newBlog->save())
        {
            //echo asset('storage/'.$event->file_link);
            
            //session()->flashMessage('check', 'Blog Created successfully!', 'success');

            return redirect()->route('blog.index');
        }

    }

    /**
     * Display the specified blog.
     */
    public function show(Blog $blog)
    {
        
        return view('blog.view', ['blog'=> $blog]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Blog $blog)
    {
        return view('blog.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Blog $blog)
    {
       
        
       /*  $request->validate([
            'title'=> 'required',
            'description'=> 'required',
            'place' => 'required',

        ]);
        $blog->title = $request->title;
        $blog->description = $request->description;
        $blog->place = $request->place;
        $blog->links = $request->links;
        $blog->save(); */
        
        

        return redirect()->route('blog.edit', $blog->id)->with('success','Blog updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Blog $blog)
    {
        $blog->delete();

        return redirect()->route('blog.index');
    }
}
