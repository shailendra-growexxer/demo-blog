<?php

use App\Models\Tag;
use App\Models\Category;

function categories ()
{
    $categories = Category::all();
   
    return $categories;
}

function tags ()
{
    $tags = Tag::all();
   
    return $tags;
}