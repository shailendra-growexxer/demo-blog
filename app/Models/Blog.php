<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    public function blog_comments()
    {
      return $this->hasMany('App\Models\Comment','blog_id','id');
    }

    public function blog_likes()
    {
      return $this->hasMany('App\Models\Like','blog_id','id');
    }
}


