@extends('layouts.app')

@section('content')  
<section class="section">
    <div class="container">
        <div class="row no-gutters-lg">
            <div class="col-12">
                <h2 class="section-title">My Blogs</h2>
                
            </div>
            <div class="col-lg-8 mb-5 mb-lg-0">
                <div class="row">
                    <div class="col-md-12">	
                         <form action="{{ route('blog.store') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="active" value="1">
                            <input type="hidden" name="added_by" value="{{Auth::user()->id}}">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                        <label for="nome">Title</label>
                                        <input type="text" name="title" class="form-control" placeholder="title" required="" value="{{ old('title') }}" autofocus>
                                        @if($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('place') ? 'has-error' : '' }}">
                                        <label for="nome">place</label>
                                        <input type="text" name="place" class="form-control" placeholder="place" minlength="6" required="">
                                        @if($errors->has('place'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('place') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('links') ? 'has-error' : '' }}">
                                        <label for="nome">Link</label>
                                        <input type="text" name="links" class="form-control" placeholder="links">
                                        @if($errors->has('links'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('links') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('categories') ? 'has-error' : '' }}">
                                        <label for="nome">Category</label>
                                        {{-- <input type="text" name="category" class="form-control" placeholder="category"> --}}
                                        <select name="categories" class="form-control" id="">
                                            @foreach(categories() as $category)
                                            <option value="{{$category->category}}">{{$category->category}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('categories'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('categories') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
                                        <label for="name">Tags(Add coma between tags)</label>
                                        <input type="text" name="tags" class="form-control" placeholder="tags">
                                        @if($errors->has('tags'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('tags') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                               
                               
                                
    
                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                        <label for="nome">image</label>
                                        <input type="file" name="image" class="form-control" >
                                        @if($errors->has('image'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
    
                                
                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                        <label for="nome">Description</label>
                                        <textarea name="description" class="form-control" placeholder="description" required=""  cols="50" rows="10">{{ old('description') }}</textarea>
                                        {{-- <input type="text" name="description" class="form-control" placeholder="description" required="" value="{{ old('description') }}"> --}}
                                        @if($errors->has('description'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6">
                                   <button type="submit" class="btn btn-primary float-right"><i class="fa fa-fw fa-plus"></i> Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            @include('layouts.right_navigation')
        </div>
    </div>
</section>
@endsection
