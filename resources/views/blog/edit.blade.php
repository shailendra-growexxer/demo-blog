@extends('layouts.app')

@section('content')  
<section class="section">
    <div class="container">
        <div class="row no-gutters-lg">
            <div class="col-12">
                <h2 class="section-title">My Blogs</h2>
                
            </div>
            <div class="col-lg-8 mb-5 mb-lg-0">
                <div class="row">
                    <div class="col-md-12">	
                         <form action="{{ route('blog.update',$blog->id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <input type="hidden" name="active" value="1">
                            <input type="hidden" name="added_by" value="{{Auth::user()->id}}">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                        <label for="nome">Title</label>
                                        <input type="text" name="title" class="form-control" maxlength="30" minlength="4" placeholder="title" required="" value="{{ $blog->title }}" autofocus>
                                        @if($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('place') ? 'has-error' : '' }}">
                                        <label for="nome">place</label>
                                        <input type="text" name="place" class="form-control" placeholder="place" minlength="6" required="" value="{{$blog->place}}">
                                        @if($errors->has('place'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('place') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('links') ? 'has-error' : '' }}">
                                        <label for="nome">Link</label>
                                        <input type="text" name="links" class="form-control" placeholder="links" value="{{$blog->links}}">
                                        @if($errors->has('links'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('links') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                               
                               
                                
    
                                {{-- <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                        <label for="nome">image</label>
                                        <input type="file" name="image" class="form-control" >
                                        @if($errors->has('image'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div> --}}
    
                                
                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                        <label for="nome">Description</label>
                                        <textarea name="description" class="form-control" placeholder="description" required=""  cols="50" rows="10">{{$blog->description}}</textarea>
                                        {{-- <input type="text" name="description" class="form-control" placeholder="description" required="" value="{{ old('description') }}"> --}}
                                        @if($errors->has('description'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6">
                                   <button type="submit" class="btn btn-primary float-right"><i class="fa fa-fw fa-plus"></i> Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            @include('layouts.right_navigation')
        </div>
    </div>
</section>
@endsection
