@extends('layouts.app')

@section('content')  
<section class="section">
    <div class="container">
        <div class="row no-gutters-lg">
            <div class="col-12">
                <h2 class="section-title">My Blogs</h2>
                <a href="{{ route('blog.create') }}" class="btn btn-sm btn-outline-primary float-right">New Blog</a>
            </div>
            <div class="col-lg-8 mb-5 mb-lg-0">
                <div class="row">
                  
                    @foreach ($blogs as $item)
                    <div class="col-md-6 mb-4">
                        <article class="card article-card article-card-sm h-100">
                            <a href="article.html">
                                <div class="card-image">
                                    <div class="post-info"> <span class="text-uppercase">{{$item->created_at}}</span>
                                        
                                    </div>
                                    <img loading="lazy" decoding="async" src="{{ asset('storage/'.$item->image) }}" alt="Post Thumbnail" class="w-100">
                                </div>
                            </a>
                            <div class="card-body px-0 pb-0">
                            {{-- <ul class="post-meta mb-2">
                                <li> <a href="#!">travel</a></li>
                                <li> <a href="#!">travel</a></li>
                            </ul> --}}
                            <h2><a class="post-title" href="{{ route('blog.show', $item->id) }}">{{$item->title}}</a>
                            </h2>
                            <p class="card-text">{{$item->description}}</p>
                            <div class="content">
                               <a class="read-more-btn" href="{{ route('blog.show', $item->id) }}">Read Full Article</a>
                               
                                <a  class="float-right m-2" title="Delete {{ $item->title}}" data-toggle="modal" data-target="#modal-delete-{{ $item->id }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash text-danger   " viewBox="0 0 16 16">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"/>
                                        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"/>
                                    </svg>
                                </a>  
                                <a class="float-right m-2" href="{{ route('blog.edit', $item->id) }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square text-primary" viewBox="0 0 16 16">
                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                                    </svg>
                                </a>
                            </div>
                            </div>
                        </article>
                      </div>

                      <div class="modal fade" id="modal-delete-{{ $item->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title"><i class="fa fa-warning"></i> Caution!!</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Do you really want to delete ({{ $item->title }}) ?</p>
                                </div>
                                
                                <form action="{{ route('blog.destroy', $item->id) }}" method="post" enctype="multipart/form-data">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                        <a href="#"><button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button></a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    




                  <div class="col-12">
                    <div class="row">
                      <div class="col-12">
                        <nav class="mt-4">
                          <!-- pagination -->
                          <nav class="mb-md-50">
                            <ul class="pagination justify-content-center">
                              <li class="page-item">
                              <a class="page-link" href="#!" aria-label="Pagination Arrow">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" viewBox="0 0 16 16">
                                  <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"/>
                                  </svg>
                              </a>
                              </li>
                              <li class="page-item active "> <a href="index.html" class="page-link">
                                  1
                              </a>
                              </li>
                              <li class="page-item"> <a href="#!" class="page-link">
                                  2
                              </a>
                              </li>
                              <li class="page-item">
                              <a class="page-link" href="#!" aria-label="Pagination Arrow">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" viewBox="0 0 16 16">
                                  <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z" />
                                  </svg>
                              </a>
                              </li>
                            </ul>
                          </nav>
                        </nav>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              @include('layouts.right_navigation')
            </div>
          </div>
        </section>


@endsection

@section('js')

@endsection
