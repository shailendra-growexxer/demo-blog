@extends('layouts.app')

@section('content')  
<section class="section">
    <div class="container">
        <div class="row no-gutters-lg">
            <div class="col-12">
                <h2 class="section-title">View Full</h2>
                
            </div>
            <div class="col-lg-8 mb-5 mb-lg-0">
                <article>
                    <img loading="lazy" decoding="async" src="{{ asset('storage/'.$blog->image) }}" alt="Post Thumbnail" class="w-100">
                    <ul class="post-meta mb-2 mt-4">
                        <li>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" style="margin-right:5px;margin-top:-4px" class="text-dark" viewBox="0 0 16 16">
                                <path d="M5.5 10.5A.5.5 0 0 1 6 10h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z" />
                                <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                                <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                            </svg> <span>{{date_format($blog->created_at,'d M, Y')}}</span>
                        </li>
                    </ul>
                    <h1 class="my-3" >{{$blog->title}}</h1>
                    <ul class="post-meta mb-4">
                        <li> <a >{{$blog->place}}</a>
                        </li>
                    </ul>
                    <h5 class="my-3">Category</h5>
                    <ul class="post-meta mb-4">
                        <li> <a >{{$blog->categories}}</a>
                        </li>
                    </ul>
                    <h5 class="my-3">Tags</h5>
                    <ul class="post-meta mb-4">
                        <li> <a >{{$blog->tags}}</a>
                        </li>
                    </ul>
                    <div class="content text-left">
                        <h1 id="heading">Description</h1>
                        <p>{{$blog->description}}</p>
                        
                        <!-- table of content -->
                        <h2 id="link">Link</h2>
                        <p><a href="{{$blog->links}}">{{$blog->links}}</a>
                        </p>
                      
                    </div>

                    
                </article>
              
            </div>

            @include('layouts.right_navigation')
        </div>
    </div>
</section>
@endsection
