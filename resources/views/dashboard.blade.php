@extends('layouts.app')

@section('content')  
<section class="section">
    <div class="container">
        <div class="row no-gutters-lg">
            <div class="col-12">
                <h2 class="section-title">Latest Articles</h2>
            </div>
            <div class="col-lg-8 mb-5 mb-lg-0">
              <div class="row">
                  
                  @php($i = 1)
                 
                  @foreach ($blogs as $item)
                    @if($i==1)
                    <div class="col-12 mb-4">
                    @else 
                    <div class="col-md-6 mb-4">
                    @endif
                    @php($i = 0)
                   
                        <article class="card article-card article-card-sm h-100">
                            <a href="article.html">
                                <div class="card-image">
                                    <div class="post-info"> <span class="text-uppercase">{{$item->created_at}}</span>
                                        
                                    </div>
                                    <img loading="lazy" decoding="async" src="{{ asset('storage/'.$item->image) }}" alt="Post Thumbnail" class="w-100">
                                </div>
                            </a>
                            <div class="card-body px-0 pb-0">
                                {{-- <ul class="post-meta mb-2">
                                    <li> <a href="#!">travel</a></li>
                                    <li> <a href="#!">travel</a></li>
                                </ul> --}}
                                <h2><a class="post-title" href="{{ route('blog.show', $item->id) }}">{{$item->title}}</a>
                                </h2>
                                <p class="card-text">{{$item->description}}</p>
                                <div class="content">
                                    <a class="read-more-btn" href="{{ route('blog.show', $item->id) }}">Read Full Article</a>
                                
                                    @if(Auth::user()->id == $item->added_by)
                                    <a  class="float-right m-2" title="Delete {{ $item->title}}" data-toggle="modal"
                                        data-target="#modal-delete-{{ $item->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                         fill="currentColor"
                                         class="bi bi-trash text-danger" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0
                                             1 .5-.5m2.5 0a.5.5
                             0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"/>
                                            <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.
                                            5a1 1 0
                             0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118
                              4 4 4.059V13a1
                              1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"/>
                                        </svg>
                                    </a>
                                    <a class="float-right m-2" href="{{ route('blog.edit', $item->id) }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                         fill="currentColor"
                                         class="bi bi-pencil-square text-primary" viewBox="0 0 16 16">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0
                                         1 .70
                                        7 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.
                                        25.2
                                        5 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0
                                         1.5-1.
                                        5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H
                                        9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                                        </svg>
                                    </a>
                                    @endif
                                </div>
                            </div>

                            <div class="card-footer px-0 pb-0">
                                <a onclick='postLike("{{$item->id}}")'><svg xmlns="http://www.w3.org/2000/svg"
                                    width="16"
                                     height="16" fill="currentColor" class="bi bi-hand-thumbs-up" viewBox="0 0 16 16">
                                    <path d="M8.864.046C7.908-.193 7.02.53 6.956 1.466c-.072 1.051-.23 2.016-.428 2.59-.
                                    125.
                                    36-.479 1.013-1.04 1.639-.557.623-1.282
                                    1.178-2.131 1.41C2.685 7.288 2 7.87 2 8.72v4.001c0 .845.682 1.464 1.448 1.5
            45 1.07.114 1.564.415 2.068.723l.048.03c.272.165.578.348.97.484.397.136.861.217 1.466.217h3.5c.937
             0 1.599-.477
                                    1.934-1.064a1.86 1.86 0 0 0 .254-.912c0-.152-.023-.312-.077-.464.201-.
                                    263.38-.578.488-.901.11-.33.172-.762.004-1.149.069-.13.12-.269.159-.403.
077-.27.113-.568.113-.857 0-.288-.036-.585-.113-.856a2 2 0 0 0-.138-.362 1.9 1.9 0 0 0 .234-1.734c-.206-.
592-.682-1.1-1.2-1.272-.847-.282-1.803-.276-2.516-.211a10 10 0 0 0-.443.05 9.4 9.4 0 0 0-.062-4.509A1.38 1.38
 0 0 0 9.125.111zM11.5 14.721H8c-.51 0-.863-.069-1.14-.164-.281-.097-.506-.228-.776-.393l-.04-.024c-.555-.339-
 1.198-.731-2.49-.868-.333-.036-.554-.29-.554-.55V8.72c0-.254.226-.543.62-.65 1.095-.3 1.977-.996 2.614-1.708.635
 -.71 1.064-1.475 1.238-1.978.243-.7.407-1.768.482-2.85.025-.362.36-.594.667-.518l.262.066c.16.04.258.143.288.255a8
 .34 8.34 0 0 1-.145 4.725.5.5 0 0 0 .595.644l.003-.001.014-.003.058-.014a9 9 0 0 1 1.036-.157c.663-.06 1.457-.054
 2.11.164.175.058.45.3.57.65.107.308.087.67-.266 1.022l-.353.353.353.354c.043.043.105.141.154.315.048.167.075.37.
 075.581 0 .212-.027.414-.075.582-.05.174-.111.272-.154.315l-.353.353.353.354c.047.047.109.177.005.488a2.2 2.2 0 0
  1-.505.805l-.353.353.353.354c.006.005.041.05.041.17a.9.9 0 0 1-.121.416c-.165.288-.503.56-1.066.56z"/>
                                  </svg></a>
                                <span class="float-right text-muted">{{$item->blog_likes->count()}} likes -
                                     {{$item->blog_comments->count()}} comments
                                    <a onclick='viewComment("comments_{{$item->id}}");'>View</a></span>
                                
                                
                            </div>

                            <!-- /.card-footer -->
                            <div class="card-footer">
                                <form action="{{ route('comments.store') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="comment-input">
                                        <img class="img-fluid img-circle img-sm"
                                         src="{{ asset('storage/'.Auth::user()->avatar) }}"
                                        width="50px" height="50px" alt="Alt Text">
                                        <!-- .img-push is used to add margin to elements next to floating images -->
                                        <input type="hidden" name="blog_id" value="{{$item->id}}">
                                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                        
                                        <input type="hidden" name="user_id" id="user_id_{{$item->id}}"
                                         value="{{Auth::user()->id}}">
                                        <input type="search" name ="comment" id="comment_{{$item->id}}"
                                         class="form-control"
                                        placeholder="Press enter to post comment">
                                       
                                          <button type="submit" class="btn btn-xs">Submit</button>
                                    </div>
                                </form>
                            </div>

                            <div id="comments_{{$item->id}}" style="display:none">
                                @foreach($item->blog_comments as $comments)
                                    <div class="card-footer">

                                        <div class="comment-input">
                                            
                                            <p><span><b>{{$comments->user->name}} : </b></span>{{$comments->comment}}</p>
                                        </div>
                                    </div>
                               
                                @endforeach
                                
                            </div>
                            
                        </article>
                      </div>

                      <div class="modal fade" id="modal-delete-{{ $item->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title"><i class="fa fa-warning"></i> Caution!!</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Do you really want to delete ({{ $item->title }}) ?</p>
                                </div>
                                
                                <form action="{{ route('blog.destroy', $item->id) }}" method="post" enctype="multipart/form-data">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                        <a href="#"><button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button></a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    
                  
                  <div class="col-12">
                    <div class="row">
                      <div class="col-12">

                        
                        <nav class="mt-4">
                          <!-- pagination -->
                          
                          <nav class="mb-md-50">
                            <ul class="pagination justify-content-center">

                             
                              <li class="page-item">
                              <a class="page-link" href="#!" aria-label="Pagination Arrow">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor"
                                  viewBox="0 0 16 16">
                                  <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0
                                   1-.708.708l-3-3a.5.5 0
                                  0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"/>
                                  </svg>
                              </a>
                              </li>
                              <li class="page-item active "> <a href="index.html" class="page-link">
                                  1
                              </a>
                              </li>
                              <li class="page-item"> <a href="#!" class="page-link">
                                  2
                              </a>
                              </li>
                              <li class="page-item">
                              <a class="page-link" href="#!" aria-label="Pagination Arrow">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor"
                                  viewBox="0 0 16 16">
                                  <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146
                                   5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0
                                  1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z" />
                                  </svg>
                              </a>
                              </li>
                            </ul>
                          </nav>
                        </nav>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              @include('layouts.right_navigation')
            </div>
          </div>
        </section>
@endsection

@section('js')
<script>


function postComment(blogId)
{
    var userId = $("#user_id_"+blogId).val();
    var commentId = $("#comment_"+blogId).val();

    var params = { blog_id: blogId, comment : commentId, user_id : userId, _token : "{{csrf_token()}}"}
    $.ajax({
        type:'post',
        url:'/comments',
        data:params,
        success:function(data) {
            //$("#msg").html(data.msg);
        }
    });
}

function postLike(blogId)
{
    var userId = $("#user_id_"+blogId).val();
    var commentId = $("#blog_id_"+blogId).val();

    var params = { blog_id: blogId, user_id : userId, _token : "{{csrf_token()}}"}
    $.ajax({
        type:'post',
        url:'/likes',
        data:params,
        success:function(data) {
            //$("#msg").html(data.msg);
        }
    });
}

function viewComment(blogCommentBoxId){
    var display = $("#"+blogCommentBoxId).css("display");
    console.log(display);
    if(display == 'none'){
        /* var update = $("#".blogCommentBoxId).css("display", "block");
        console.log($("#"+blogCommentBoxId).css("display")); */
        document.getElementById(blogCommentBoxId).style.display = "block";
    } else {
        document.getElementById(blogCommentBoxId).style.display = "none";
    }
}

/* function updateCommentsOfBlog(blogId)
{


    var oneComment = '<div class="card-footer card-comment comment-list">
                               
                                <img class="img-circle img-sm" src="{{ asset('storage/'.Auth::user()->avatar) }}"
                                 width="20px" height="20px" alt="User Image">
            
                                <div class="comment-text">
                                    <span class="username">
                                        Maria Gonzales
                                        <span class="text-muted float-right">8:03 PM Today</span>
                                    </span><!-- /.username -->
                                    It is a long established fact that a reader will be distracted
                                    by the readable content of a page when looking at its layout.
                                </div>
                                <!-- /.comment-text -->
                            </div>';
} */
</script>
@endsection