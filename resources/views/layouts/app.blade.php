<!DOCTYPE html>

<!--
 // WEBSITE: https://themefisher.com
 // TWITTER: https://twitter.com/themefisher
 // FACEBOOK: https://www.facebook.com/themefisher
 // GITHUB: https://github.com/themefisher/
-->

<html lang="en-us">

<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>
	
	
	<meta name="description" content="This is meta description">
	<meta name="author" content="Shailendra">
	<link rel="shortcut icon" href="/theme/images/favicon.png" type="/theme/image/x-icon">
	<link rel="icon" href="/theme/images/favicon.png" type="/theme/image/x-icon">
  
  <!-- theme meta -->
  <meta name="theme-name" content="reporter" />

	<!-- # Google Fonts -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Neuton:wght@700&family=Work+Sans:wght@400;500;600;700&display=swap" rel="stylesheet">

	<!-- # CSS Plugins -->
	<link rel="stylesheet" href="/theme/plugins/bootstrap/bootstrap.min.css">

	<!-- # Main Style Sheet -->
	<link rel="stylesheet" href="/theme/css/style.css">

	<!-- # custom Style Sheet -->
	<link rel="stylesheet" href="/theme/css/custom.css">
	@yield('css')
</head>

<body>

    @include('layouts.header')

<main>
	@if(Session::has('flash_message'))
                    
		<div class="{{ Session::get('flash_message')['class'] }}" style="padding: 10px 20px;" id="flash_message">
			<div style="color: #fff; display: inline-block; margin-right: 10px;">
				{!! Session::get('flash_message')['msg'] !!}
			</div> 
		</div>

	@endif 
    @yield('content')
</main>

@include('layouts.footer')


<!-- # JS Plugins -->
<script src="/theme/plugins/jquery/jquery.min.js"></script>
<script src="/theme/plugins/bootstrap/bootstrap.min.js"></script>

<!-- Main Script -->
<script src="/theme/js/script.js"></script>


@yield('js')
</body>
</html>
