<header class="navigation">
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light px-0">
        <a class="navbar-brand order-1 py-0" href="/dashboard">
          <img loading="prelaod" decoding="async" class="img-fluid" src="/theme/images/logo.png" alt="Reporter Hugo">
        </a>
        <div class="navbar-actions order-3 ml-0 ml-md-4">
          <button aria-label="navbar toggler" class="navbar-toggler border-0" type="button" data-toggle="collapse"
            data-target="#navigation"> <span class="navbar-toggler-icon"></span>
          </button>
        </div>
        <form action="{{ route('dashboard') }}" method="GET" class="search order-lg-3 order-md-2 order-3 ml-auto">
          <input id="search-query" name="search" type="search" placeholder="Search..." autocomplete="off">
        </form>
        
        <div class="collapse navbar-collapse text-center order-lg-2 order-4" id="navigation">
          <ul class="navbar-nav mx-auto mt-3 mt-lg-0">
            <li class="nav-item"> 
              <a class="nav-link" href="/dashboard">Home</a>
            </li>
            <li class="nav-item dropdown"> 
              <a class="nav-link dropdown-toggle" href="#" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                My Blogs
              </a>
              <div class="dropdown-menu"> 
                <a class="dropdown-item" href="/blog">My Blogs</a>
                
              </div>
            </li>
            {{-- <li class="nav-item"> <a class="nav-link" href="contact.html">Contact</a>
            </li> --}}
            <li class="nav-item dropdown"> 
              <a class="nav-link dropdown-toggle" href="#" role="button"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Profile
              </a>
              <div class="dropdown-menu"> 
                  <a class="dropdown-item" href="travel.html">Edit Profile</a>
                  
                  <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-dropdown-link :href="route('logout')"
                            onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-dropdown-link>
                </form>
                  
              </div>
             </li>
          </ul>
        </div>
      </nav>
    </div>
  </header>