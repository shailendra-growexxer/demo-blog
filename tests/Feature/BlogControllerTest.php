<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Blog;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BlogControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     */

    use RefreshDatabase;

    public function test_example(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    // User can create a blog

    public function test_authenticated_users_can_create_blog(): void
    {
        // Create user
        $user = User::factory()->create();

        // Login User
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => "password",
        ]);

        $this->assertAuthenticated();

        // Create new blog

        $blog = Blog::factory()->create();
        if ($blog) 
        {
            $response->assertRedirect(RouteServiceProvider::HOME);
        } else
        {
            
        }
        
        
    }

    // User can view a blog
    public function test_authenticated_users_can_view_a_blog(): void
    {
        // Create user
        $user = User::factory()->create();

        // Login User
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => "password",
        ]);

        $this->assertAuthenticated();

        // Create new blog

        $blog = Blog::factory()->create();

        $response = $this->actingAs($user)->get('/blog/'.$blog->id);

        $response->assertStatus(200);


    }

    // User can edit a blog

   /*  public function test_authenticated_users_can_edit_a_blog(): void
    {
        // Create user
        $user = User::factory()->create();

        // Login User
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => "password",
        ]);
        
        $this->assertAuthenticated();

        $blog = Blog::factory()->create();

        $response = $this->actingAs($user)->put('/blog/' . $blog->id, ['title' => 'Top 7 Reasons to Visit Denver this Summer']);
        $response->assertStatus(200);
    } */
}
