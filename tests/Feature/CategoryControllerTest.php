<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Category;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     */

    use RefreshDatabase;

    public function test_example(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    // User can create a blog

    public function test_authenticated_users_can_create_category(): void
    {
        // Create user
        $user = User::factory()->create();

        // Login User
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => "password",
        ]);

        $this->assertAuthenticated();

        // Create new blog

        $category = Category::factory()->create();
        if ($category) 
        {
            $response->assertRedirect(RouteServiceProvider::HOME);
        } else
        {
            
        }
        
        
    }

    // User can view a blog


    // User can edit a blog

   /*  public function test_authenticated_users_can_edit_a_blog(): void
    {
        // Create user
        $user = User::factory()->create();

        // Login User
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => "password",
        ]);
        
        $this->assertAuthenticated();

        $blog = Blog::factory()->create();

        $response = $this->actingAs($user)->put('/blog/' . $blog->id, ['title' => 'Top 7 Reasons to Visit Denver this Summer']);
        $response->assertStatus(200);
    } */
}
